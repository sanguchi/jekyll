---
layout: page
title: About
permalink: /about/
---

Blog simple usando el tema jekyll-new
Codigo fuente del tema:
{% include icon-github.html username="jglovier" %} /
[jekyll-new](https://github.com/jglovier/jekyll-new)

Codigo fuente de Jekyll:
{% include icon-github.html username="jekyll" %} /
[jekyll](https://github.com/jekyll/jekyll)
